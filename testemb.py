import numpy as np
import torch

from model import AEVB


model = AEVB(50, 5)
# ckpt = torch.load("fifafirst/first-try/checkpoint-epoch100.pth")
ckpt = torch.load("saved/models/FIFA_VAE/0109_023554/checkpoint-epoch50.pth")

model.load_state_dict(ckpt['state_dict'])
model.eval()

with open("pl_above_70.csv", 'r') as f:
    data = f.readlines()

data = [x.strip().split(",")[:50] for x in data]
data = [[float(y) if y != "" else 0 for y in x] for x in data]


def compare(idx1: int, idx2: int):
    player1 = torch.unsqueeze(torch.from_numpy(np.asarray(data[idx1], dtype=np.float32)), 0)
    player2 = torch.unsqueeze(torch.from_numpy(np.asarray(data[idx2], dtype=np.float32)), 0)

    z1 = np.linalg.norm(model.encode(player1)[0][0].detach().numpy())
    z2 = np.linalg.norm(model.encode(player2)[0][0].detach().numpy())
    l2 = np.linalg.norm(z2-z1)
    return l2


def main():
    p1 = input("Enter Player 1 Index: ")
    p2 = input("Enter Player 2 Index: ")
    dist = compare(int(p1), int(p2))
    print(f"Player 1 and Player 2 are : {(1-dist)*100 : .2f}% similar!")
    print()


if __name__ == "__main__":
    while 1:
        main()
