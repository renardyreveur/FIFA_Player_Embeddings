import argparse
import sys
import warnings
from collections import OrderedDict

import torch.utils.mobile_optimizer

sys.path.append('./')
import model


def main(cfg):
    # Get Model Class
    model_class = getattr(model, cfg.model)

    # Load state dict from checkpoint
    print("Checkpoint Provided, using trained weights!")
    ckpt = torch.load(cfg.checkpoint)

    # Load model arguments
    model_config = dict(ckpt['config']['arch']['args'])
    if len(model_config.keys()) > 0:
        print(
            "Model Configuration:\n{}\n".format("\n".join([str(k) + " : " + str(v) for k, v in model_config.items()])))
    else:
        print("Model Configuration:\n default")

    # Instantiate model and load state dictionary on it
    model_instance = model_class(**model_config)
    state_dict = ckpt['state_dict']

    # If state_dict was saved from DataParallel Model, change state_dict
    if list(state_dict.keys())[0][:6] == "module":
        new_state_dict = OrderedDict()
        for k, v in state_dict.items():
            name = k[7:]
            new_state_dict[name] = v
        state_dict = new_state_dict

    # Load state dict
    model_instance.load_state_dict(state_dict)

    # Model Evaluation Mode
    model_instance.eval()

    # Create Dummy Input and Output for Testing
    x = torch.randn(*cfg.input)
    torch_out = model_instance(x)

    # Quantize model if set to quantize
    if cfg.quantize:
        model_instance = torch.quantization.convert(model_instance)

    # Convert to TorchScript Model
    if cfg.trace:
        model_instance = torch.jit.trace(model_instance, example_inputs=x)
    else:
        model_instance = torch.jit.script(model_instance)

    # Optimize for mobile if set to optimize
    if cfg.optimize:
        model_instance = torch.utils.mobile_optimizer.optimize_for_mobile(model_instance)
        print("Optimizing the model for mobile seems to use Named Tensors which are still experimental, use with care!")

    # Save Torchscript model to file
    torch.jit.save(model_instance, cfg.name)
    print("Torchscript model saved as {}".format(cfg.name))

    warnings.filterwarnings("ignore")

    # Check validity
    scripted_model = torch.jit.load(cfg.name)
    script_out = scripted_model(x)

    valid = torch.allclose(torch_out, script_out, rtol=1e-05, atol=1e-08)
    print("The converted model is close to the original model: {}!".format(valid))

    if not valid:
        print("Difference: {} \n Done!".format(torch.abs(torch_out - script_out).sum().item()))
    else:
        print("All good!!")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="TorchScript Converter")
    parser.add_argument('-m', '--model', help='Model Class Name', required=True)
    parser.add_argument('-c', '--checkpoint', help='Path to Trained Checkpoint', required=True)
    parser.add_argument('-i', '--input', nargs="+", help='Input Size', type=int, required=True)
    parser.add_argument('-q', '--quantize', help='Quantization', required=False, action='store_true')
    parser.add_argument('-o', '--optimize', help='Optimize for Mobile', required=False, action='store_true')
    parser.add_argument('-t', '--trace', help='Use JIT trace instead of script', required=False, action='store_true')
    parser.add_argument('-n', '--name', help="Torchscript Model Name", required=True)

    args = parser.parse_args()
    main(args)
