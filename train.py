import argparse
import collections
import torch
import wandb
import numpy as np
import data_loader.data_loaders as module_data
import model.loss as module_loss
import model.metric as module_metric
import model.model as module_arch
from parse_config import ConfigParser
from trainer import Trainer
from utils import to_dict, flatten, load_trained_state_dict

import torch.multiprocessing as mp
import torch.distributed as dist
import os

# fix random seeds for reproducibility
SEED = 123
torch.manual_seed(SEED)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False
np.random.seed(SEED)


def init_process(rank, size, backend='nccl'):
    """ Initialize the distributed environment. """
    os.environ['MASTER_ADDR'] = '127.0.0.1'
    os.environ['MASTER_PORT'] = '29500'
    dist.init_process_group(backend, rank=rank, world_size=size)


def main(rank, config, world_size):
    if config['distributed']:
        init_process(rank, world_size)
        print(f"Rank {rank + 1}/{world_size} process initialized.\n")

    if rank == 0:
        # WandB Resume Handling
        resume = False
        if config.resume is not None:
            resume = True

        if config['track_experiment']:
            # Send Config to WandB
            wandb.init(config=flatten(to_dict(config.config)), **flatten(config['wandb']), resume=resume)

        # Logger
        logger = config.get_logger('train')
    # dist.barrier()

    # setup data_loader instances
    data_loader = config.init_obj('data_loader', module_data,
                                  distributed=config['distributed'], rank=rank, world_size=world_size)
    valid_data_loader = data_loader.split_validation()

    # build model architecture, then print to console
    model = config.init_obj('arch', module_arch)
    if config['trained_weights'] != "":
        if not os.path.exists(config['trained_weights']):
            print("The trained weights file does not exist! Check your config file!")
            return -1
        try:
            load_trained_state_dict(model, config['trained_weights'])
        except AttributeError:
            print("The trained weights have keys that do not exist in the model, check your model and trained weights "
                  "configuration!")
            return -1

    # get function handles of loss and metrics
    criterion = getattr(module_loss, config['loss'])
    metrics = [getattr(module_metric, met) for met in config['metrics']]

    # build optimizer, learning rate scheduler. delete every lines containing lr_scheduler for disabling scheduler
    trainable_params = filter(lambda p: p.requires_grad, model.parameters())

    if 'module' not in config['optimizer']:
        module = torch.optim
    else:
        module = config['optimizer']['module']
    optimizer = config.init_obj('optimizer', module, trainable_params)

    lr_scheduler = config.init_obj('lr_scheduler', torch.optim.lr_scheduler, optimizer)

    if rank == 0:
        logger.info(model)
        if config['track_experiment']:
            # Send model to WandB
            wandb.watch(model)
    # dist.barrier()

    # Turn off AMP if CUDA not available
    if not torch.cuda.is_available() or config['n_gpu'] == 0:
        if config['trainer']['amp']:
            logger.warning("Warning: CUDA device not available! Automatic Mixed Precision is disabled.")
            config['trainer']['amp'] = False

    # Trainer
    trainer = Trainer(model, criterion, metrics, optimizer,
                      config=config,
                      data_loader=data_loader,
                      valid_data_loader=valid_data_loader,
                      lr_scheduler=lr_scheduler,
                      rank=rank,
                      world_size=world_size)

    trainer.train()


if __name__ == '__main__':
    args = argparse.ArgumentParser(description='PyTorch Template')
    args.add_argument('-c', '--config', default=None, type=str,
                      help='config file path (default: None)')
    args.add_argument('-r', '--resume', default=None, type=str,
                      help='path to latest checkpoint (default: None)')
    args.add_argument('-d', '--device', default=None, type=str,
                      help='indices of GPUs to enable (default: all)')

    # custom cli options to modify configuration from default values given in json file.
    CustomArgs = collections.namedtuple('CustomArgs', 'flags type target')
    options = [
        CustomArgs(['--lr', '--learning_rate'], type=float, target='optimizer;args;lr'),
        CustomArgs(['--bs', '--batch_size'], type=int, target='data_loader;args;batch_size'),
    ]
    cfg = ConfigParser.from_args(args, options)

    if cfg['distributed']:
        WORLD_SIZE = cfg['n_gpu']
        mp.spawn(main, args=(cfg, WORLD_SIZE), nprocs=WORLD_SIZE, join=True)
    else:
        main(0, cfg, 1)
