import pandas as pd
from datetime import datetime


def ohe(entry, full):
    result = [0]*len(full)
    try:
        entries = entry.split(",")
        entries = [x.strip() for x in entries]
        for en in entries:
            result[full.index(en)] = 1
    except Exception:
        pass
    return result


def onehotdf(column):
    catlist = [x for x in column]
    catstrs = [x.split(",") for x in catlist if not isinstance(x, float)]
    catset = []
    for it in catstrs:
        catset.extend(it)
    catset = [x.strip() for x in catset]
    column = column.apply(ohe, full=list(set(catset)))

    return column.apply(pd.Series, index=list(set(catset)))


def filter_fifa(dataframe, ovr):
    # Read player data from a CSV file
    player_data = pd.read_csv(dataframe)

    # Filter by OVR
    pl_above_ovr = player_data[player_data['overall'] >= ovr]

    # Drop unnecessary columns
    with open("../../dropcols.txt", 'r') as f:
        dropcols = f.readlines()
    dropcols = [x.strip() for x in dropcols]
    pl_above_ovr = pl_above_ovr.drop(dropcols, axis=1)
    # Encode categorical variables
    ptag = onehotdf(pl_above_ovr['player_tags'])
    ptrait = onehotdf(pl_above_ovr['player_traits'])
    ppos = onehotdf(pl_above_ovr['player_positions'])

    ptag_order = ptag.columns
    ptrait_order = ptrait.columns
    ppos_order = ppos.columns

    pl_above_ovr = pd.get_dummies(pl_above_ovr,
                                  columns=['club_team_id', 'league_name', 'nationality_id', 'nation_team_id', 'preferred_foot', 'work_rate', 'body_type'],
                                  prefix=['club', 'league', 'nation', 'nat_team', 'pfoot', 'workrate', 'body'])
    pl_above_ovr = pl_above_ovr.join(ptag)
    pl_above_ovr = pl_above_ovr.join(ptrait)
    pl_above_ovr = pl_above_ovr.join(ppos)
    pl_above_ovr = pl_above_ovr.drop('player_tags', axis=1)
    pl_above_ovr = pl_above_ovr.drop('player_traits', axis=1)
    pl_above_ovr = pl_above_ovr.drop('player_positions', axis=1)

    # Normalize Columns
    pl_above_ovr['overall'] = pl_above_ovr['overall']/100
    pl_above_ovr['potential'] = pl_above_ovr['potential']/100
    pl_above_ovr['wage_eur'] = (pl_above_ovr['wage_eur'] - min(pl_above_ovr['wage_eur']))/(max(pl_above_ovr['wage_eur']) - min(pl_above_ovr['wage_eur']))
    pl_above_ovr['age'] = (pl_above_ovr['age'] - pl_above_ovr['age'].mean()) / pl_above_ovr['age'].std()
    pl_above_ovr['dob'] = pl_above_ovr['dob'].apply(lambda x: datetime.timestamp(datetime.strptime(x, '%Y-%m-%d')))
    pl_above_ovr['dob'] = (pl_above_ovr['dob'] - min(pl_above_ovr['dob']))/(max(pl_above_ovr['dob']) - min(pl_above_ovr['dob']))
    pl_above_ovr['height_cm'] = (pl_above_ovr['height_cm'] - min(pl_above_ovr['height_cm']))/(max(pl_above_ovr['height_cm']) - min(pl_above_ovr['height_cm']))
    pl_above_ovr['weight_kg'] = (pl_above_ovr['weight_kg'] - min(pl_above_ovr['weight_kg']))/(max(pl_above_ovr['weight_kg']) - min(pl_above_ovr['weight_kg']))
    pl_above_ovr['weak_foot'] = pl_above_ovr['weak_foot']/5
    pl_above_ovr['skill_moves'] = pl_above_ovr['skill_moves']/5
    pl_above_ovr['international_reputation'] = pl_above_ovr['international_reputation']/5
    with open('../../stats.txt', 'r') as f:
        stats = f.readlines()
    stats = [x.strip() for x in stats]
    for st in stats:
        pl_above_ovr[st] = pl_above_ovr[st]/100

    return pl_above_ovr, ptag_order, ptrait_order, ppos_order


if __name__ == "__main__":
    pl_above_70, tago, traito, poso = filter_fifa("../../data/players_22.csv", 70)
    # print(pl_above_70.info())
    # print(pl_above_70.iloc[[5]])
    pl_above_70.to_csv("../../pl_above_70.csv")
    print(tago)
    print(traito)
    print(poso)