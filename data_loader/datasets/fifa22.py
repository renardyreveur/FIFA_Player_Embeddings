import torch
from torch.utils.data.dataset import Dataset


class FIFA22Data(Dataset):
    def __init__(self):
        super().__init__()
        with open("pl_above_70.csv", 'r') as f:
            pl_above_70 = f.readlines()
        self.f_dataset = [x.strip() for x in pl_above_70]
        self.f_dataset = [x.split(",")[:50] for x in self.f_dataset]
        self.f_dataset = [[float(x) if not x == "" else 0 for x in y] for y in self.f_dataset]

    def __len__(self) -> int:
        return len(self.f_dataset)

    def __getitem__(self, idx):
        return torch.Tensor(self.f_dataset[idx])


if __name__ == "__main__":
    d = FIFA22Data()
    print(d[1])