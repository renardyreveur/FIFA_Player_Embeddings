import h5py
import numpy as np
import torch

from PIL import Image
from torch.utils.data.dataset import Dataset


class MNIST_H5(Dataset):
    def __init__(self, h5_path, transform):
        super().__init__()
        print("Loading MNIST H5...")

        self.h5_path = h5_path
        self.transforms = transform

        with h5py.File(self.h5_path, 'r') as file:
            self.len_data = file['image'].shape[0]
            self.images = file["image"][:]
            self.labels = file["label"][:]

    def __len__(self):
        return self.len_data

    def __getitem__(self, idx):
        image = np.array(self.images[idx]).astype("uint8")
        label = int(self.labels[idx])

        image = Image.fromarray(image)

        if self.transforms:
            image = self.transforms(image)

        return image, label, idx
