import time

import cv2
import h5py
import numpy as np


def h5test(h5_file, num_img=1000, visualize=False):
    # Open generated HDF5 file
    file = h5py.File(h5_file, 'r+')
    print(f"Dataset is of shape: {file['image'].shape}")
    file.close()

    avg = 0
    # For the amount of number specified by the function argument
    for i in range(num_img):
        start = time.time()
        # Get data from the generated file
        with h5py.File(h5_file, 'r') as file:
            image = np.array(file["image"][i]).astype("uint8")
            label = file["label"][i]
            idx = file["idx"][i]
        end = time.time()
        avg += end-start

        if visualize:
            cv2.imshow("image", image)
            print(f"Label: {label}, Idx: {idx}")
            cv2.waitKey(0)

    print(f"Took an average of {avg/num_img} seconds to read data")

    print(f"Sample Image shape {image.shape}")
    print(f"Sample label: {label}")
