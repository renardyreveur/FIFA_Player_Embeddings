# Background Material

## Information Theory

### What is **information**?

One can come up with many different definitions for the term 'information', but a useful way to think about it is as *"something that is useful in making decisions in a state of uncertainty"*. Like the popular quote, commonly attributed to Shannon goes,: **"Information is the resolution of uncertainty"**. This is crucial as it provides a reference frame where we can link 'information' with 'probability'.

### **Quantification of Information**

So how can we quantify the amount of information content embedded in an informative entity?

By the definition above, if the retrieval of an 'information' greatly affects the final decision, then that information must have a lot of value or "content". This is rephrased as the question that follows:

*"How suprised am I about the given information to be that specific information?"*

Therefore, the amount of content that an information unit carries must have a relationship with the possible choices of the event associated with said information. The more choices there are, the more uncertainty there is to that event; and thus the realization of a choice out of that uncertainty has more effect on the decision making process(uncertainty $\propto$ information content). 

Given a sequence of events, the information content is better modelled to *grow linearly with the length of sequence* rather than exponentially. (Despite the fact that the actual 'selection count' of a sequence of selections are given as a product (over all steps) of the number of selections per step.) In the sense that a 'message' being transferred over a transmission line, the information content is constant per decoding step, and just because something appeared later in the sequence, it does not have more 'informational power' to the receiver. (Sure, there could have been tension building up and a final answer sort of thing can actually contain the main gist of the whole sequence, but generally speaking, we can say that information content is linear to the sequence length.).

In statistics we assign the word **'Random Variable'** to a variable that takes a specific value depending on a probability. This is a great way to model an 'Event with possible selections'. Thus we can connotate the 'choices of an event' to 'the distribution of probabilities of a random variable being assigned a specific value'. Generally we tend to ascribe the uniform distribution to the 'choice of selections', but it can be extended for any type of distributions. The crux of it all is that the less probable a choice is the more impact it has on the decision(more information content). Here we can summarise the basic relationship between information content and probability.

$$ \text{Information Content} \propto \frac{1}{P(X=x)} $$

where $X$ is a random variable.

On top of this if $P(X=x) = 1$ this is a 'certainty', i.e choice of selection is deterministic. Thus there is no 'information' in there. It does not affect the decision making process at all. On the other hand, if there are more selections, or as the probability of an event decreases, the uncertainty in the system rises and the realisation for that event will have a massive effect on the decision making process. Thus a constraint on the relationship arises as

$$ \text{Information Content} = 0 \text{\quad iff \quad} P(X=x) = 1 $$

and

$$ \lim_{P(X=x)\to 0} \text{Information Content} = \infty $$

As a 'quantity' we can also assert that 'Information Content' is a non-negative measure

$$ \text{Information Content} \geq 0 $$

Relating back to the linearly increasing 'Information Content' to a sequence of events, speaking with the 'random variable' formulation of the problem, we can say that the information content in two independent events X and Y happening (thus the joint probability of X=x and Y=y) is:

$$ IC(P(X=x, Y=y)) = IC(P(X=x)P(Y=y)) = IC(P(X=x)) + IC(P(Y=y)) $$

where $IC$ denotes Information Content. Without this condition, the function space opens up to whatever function that is asymptotic at P(X=x) = 0 to infinity, and that crosses the x axis at 1. (e.g $-log(x)$, $tan((2-x)\pi)$, $\frac{1}{x} - 1$ etc.)

The only function to satisfy all the conditions and relationships above is

$$ -log(P(X=x)) $$

> Thus the information content of a random variable $X$ to be assigned a value $x$ with given probability $P(X=x)$ is given as $-log(P(X=x))$

The expected value of Information Content over the whole system is termed **Entropy** (noted as $H$ below) by C. Shannon, and thus has the form:

$$ H(x) = - \sum_{x \in X}P(x) \ log(P(x))$$ 


### **Comparing Information Content**

A useful application of having a quantity to denote the average information content of a system is that we can compare systems with their information content. This is especially useful when comparing acquired real-life data to a predisposed model of the data source. We can try and calcuate the 'average information loss' in trying to model the 'truth(data)' with our model.

One way you could do it is to just subtract the information content between two systems(data, model), and get a quantified value, but this doesn't mean much. It's just an absolute difference of the 'mass' of the information content, and it doesn't relate the two systems together. A better way to subtract the information content of two systems is to have a 'base' system and see how using a different model on the base system accrues information loss.

Consider the following: Let us try and find the absolute difference between the entropy of the base system and the entropy of the model but 'applied' to the base system. (Subtract the true entropy from the model based entropy (to just remove the minus sign in the front))

$$ \sum_{x \in X}P(x)log(P(x)) - \sum_{x \in X}P(x)log(Q(x)) $$

The first term in the expression above is the so called 'True entropy' of the system($P(X)$), derived from data. $Q(X)$ is the 'model' for $P(X)$. The second term, then in laymans term becomes, **"average information content if the amount of information for each event is given by the model, but it 'happens' like in real life(truth)"**. Therefore, the expression above can be said to express the *'average information loss when using $Q(X)$ to model $P(X)$'*. Thus it is a metric of some sort that defines the **'divergence'** (if we're being pedantic, it's not a metric cause it's not symmetric nor does it follow the triangle inequality) of a probability distribution from another.

Collecting the $P(X)$ term gives

$$ D_{KL}(P || Q) = \sum_{x \in X}P(X) \ log\left(\frac{P(X)}{Q(X)}\right)$$

This is the **Kullback-Leibler Divergence** or the relative entropy from $Q$ to $P$.

Going back to that second term in the expression above, that is actually called the **Cross Entropy of $Q$ relative to $P$**

$$ H(P, Q) = - \sum_{x \in X}P(X) \ log(Q(X)) $$

These are typically used in optimization problems, where we evaluate a learning model $Q$ to a reference distribution $P$. The Kullback-Leibler Divergence and Cross entropy both take minimal values when $P=Q$ where $D_{KL} = 0$ and $H(P,Q) = H(P)$. In Deep Learning, especially in classification problems, the classification loss is given by the Cross Entropy. Cross Entropy as seen above takes less calculations than the Kullback-Leibler divergence (don't need to calcuate the Entropy of the data), and with a label typically in the dirac form or one-hot encoded form, the 'true entropy' actually becomes 0 or very close to 0. Thus the minimization target is 0 even using Cross Entropy for classfication problems.

---

## Bayesian Inference

There are two interpretations on the concept of 'probability' of an event. One is the Frequentist view of interpreting probability as the limiting ratio of the results of an event(action) being repeated into infinity. The other is the Bayesian view of regarding probability as a description of my knowledge about the system and the lack of sufficient information to make judgements. It models knowledge in light of not enough information as 'belief' (which can change with the availability of further information). 

As such, Bayesian Inference is all about updating probabilities as more evidence or information gives available. Thus whatever that is inferred will change as the probabilities are updated with new evidence. 

> So naturally, the question arises: **How do we update the probabilities?**

New knowledge or evidence is some sort of a constraint or condition on the system. Something has already happend or some state is at that specific state. Relating to the information theory approach in the previous section, we can say that attaining new information means that an uncertainty has been resolved. We already have a way of expressing this kind of relationship: "$A$ **given that** $B$ has happend". In mathematical notation we write it with a $|$ bar.

$$ P(A \ | \ B) $$

To evaluate the above expression, let us think about what it means. 

![conditional_probability](conditional.jpg)

In the picture above, let's think about our entire system as the left most Venn diagram. Everything that can happen is contained in the rectangle. We can think of a specific event such as $A$ or $B$ happening with probability in proportion to their area inside the rectangle. In that sense, the *probability of an event is just the area of the circle of the event*. 

Let us say that we are then given knowledge that event $B$ has happened. Then we can think **as if the boundaries of our system has collapsed in to the circle of $B$**. The addition of new information have removed the other areas of uncertainty. It is from here that we try and find the probability of event $A$ happening. From the picture we can directly see that the event of $A$ happening when our system has collapsed in to the bounds of $B$ is $P(A \cap B)$. So in terms of the probability that event A happens given that B has happened, **we take the ratio of the collapsed system to the event in question like we would have done with the original system to an event in question.** (Like the ratio between the original system and event B happening being $1:P(B)$, event $P(A \cap B)$ happening given $P(B)$ would be $P(B) : P(A \cap B)$)

$$P(A \ | \ B) = \frac{P(A \cap B)}{P(B)}$$

and thus

$$P(B \ | \ A) = \frac{P(B \cap A)}{P(A)}$$

As we know that $P(A \cap B) = P(B \cap A)$, we can equate the rest of the two equations together getting:

$$ \frac{P(A \ | \ B)}{P(A)} = \frac{P(B \ | \ A)}{P(B)} $$

This is **Baye's Theorem**, and it provides us a way to update the probabilities in light of new information. Rearranging the above equation results in the common form of Baye's Theorem:

$$ P(A \ | \ B) = \frac{P(B \ | \ A)P(A)}{P(B)} $$

In words: The posterior probability of event $A$ with evidence $B$ ($P(A \ | \ B)$) is given by the ratio between the product of the **likelihood of evidence $B$ for observed result $A$ ($P(B \ | \ A)$) and the prior probability of $A$ ($P(A)$)** without evidence to the **marginal probability of the evidence P(B)**.

The update happens by continuing to substitute $P(A | B)$ in place of $P(A)$ for each new information received.

Typically the equation is not used with single probabilities, but with **probability distributions**. So we are talking about prior distributions, posterior distributions and marginal distributions.

With that in mind, the model form of Bayes theorem is as follows:

$$ P(\Theta \ | \ data) = \frac{P(data \ | \ \Theta) P(\Theta)}{P(data)} $$

for $\Theta$ being a set of parameters or values we are interested in, and $data$ the set of observations we have.

> We get an update of our estimate on the true values of the parameters with new data! We can calculate the posterior distribution of our parameters using only our prior beliefs updated with our likelihood.

Typically, $P(data)$, which is the marginal distribution of the evidence acquired, is hard to calculate. It is also the 'normalisation' constant in making the posterior distribution a 'true probability distribution', so in fact in many times it is not required for inference. Still, there are cases where you need to at least approximate the value. As the name suggests(marginal means we sum over one side of the joint probability variables), we marginalise the term and try and calculate using easier values that we can already handle (From the numerator).

$$ P(data) = \int_{\Theta} P(data | \Theta) \ P(\Theta) \  d\Theta $$ 

We sum up over all the possible hypotheses(or set of parameters) to get the distribution of the evidence.

This integral is usually very hard to analytically solve, so methods such as **Monte Carlo, Variational Bayes, etc. are used to approximate it**.

---

### Other notes on Probability

- **Probability** gives the uncertainty of an observation given a distribution. **Likelihood** gives the prospect of a distribution given an observation.

- **Bayesian Infernece**: Say we have some prior distribution on a value of interest. We first gather data by observing a few samples of the random variable in interest. We derive a likelihood distribution from the observations (maximum likelihood). These two distribution multiplied gives the unnormalised posterior distribution of the value now updated with evidence. Here we can calculate the expected value, variance, or get the **Maximum a posteriori** estimate of the value.

- **Conjugate** distributions make life easier (posterior distribution fmaily = prior distribution family). Choose for likelihood and priors such that they are conjugate.

- **Priors act as regularizers** to the observed data. Relative uncertainty between two data gives weight between prior and likelihood

<br>

---
---

<br>
  

## Variational Bayesian Methods

According to Wikipedia, Variational Bayesian methods are **"a family of techniques for approximating intractable integrals arising in Bayesian inference and machine learning"**. The *intractable integrals* that they are talking about are the crazy ass integrals we saw on the marginal distribution of the evidence from the previous sections.

In approximating the posterior probabilities, Variational Bayes is an alternative to **Monte Carlo** methods(repeated random sampling to obtain numerical results) 

> **Monte Carlo** - Define domain of possible inputs, generate inputs randomly from a probability distribution, perform a deterministic calculation on the inputs, aggregrate the results


The posterior distribution over a set of unobserved variables $Z = \{Z_1, Z_2, ..., Z_n\}$ given some data $X$ is approximated by a **variational distribution** $Q(Z)$

$$ P(Z \ | \ X) \approx Q(Z) $$

We set the distribution of $Q(Z)$ to be something simpler such as the family of Gaussian distributions, and just try and make it as close to the true posterior. This makes things tractable, and we can get a good enough approximation.

The similarity is measured using a dissmilarity measure $d(Q; P)$, and thus the $Q(Z)$ that minimizes $d(Q; P)$ is used for inference. The most common dissimilarity measure used is the **Kullback-Leibler divergence** we discussed above! 
> but $D_{KL}(Q || P)$ not the other way around!

If one ponders about that for a while, we can reach a conclusion like this. In forward KL ($D_{KL}(P || Q)$) the entropies are weighted by $P$ which means when optimizing $Q$ for $P$, there are no penalties for $Q$ when $P=0$. **The term $log\left(\frac{P}{Q} \right)$ will contribute to the overall KL divergence for all $P > 0$**

On the other hand, for reverse KL divergence, $Q$ is now the weight, and thus **if $Q=0$, even if $P > 0$, it isn't penalised!!** It is more focussed on areas where $Q >0$ and it will try and minimize the divergence in that region as much as possible. 

This means that optimization with forward KL can **capture the *generality of the true distribution*, but it might not specifically match up to the shape of specifc parts** of the true posterior distribution.

Conversely, optimization carried forward with reverse KL can **capture *local aspects* of the true posterior distribution pretty well, but might have under-represented, or mis-represented areas of the true distribution** due to the constraint of using a 'simpler distribution' to approximate.

> In that case *which direction of Kullback-Leibler divergence is suitable for Variational Bayes?*

We already have the answer from above, the reverse one! But really it depends! Every problem has it's own little niche and own little constraints and aim. We can naturally see that the reverse divergence is more useful as it can capture the shape of the distribution more accurately albeit locally. We can always understand that the approximation is at best a locally optimal approximation without further information or conditions. The new approximating model will at least produce samples that *make sense* (in real life) rather than just trying to generally minimize the information content mathematically.


### **Evidence Lower Bound (ELBO)**

Given that $ P(Z | X) = \frac{P(X, Z)}{P(X)} $ , the Kullback-Leibler divergence can be written as 

$$ \begin{align*} D_{KL}(Q || P) &= \sum_{Z}Q(Z)\left[log\frac{Q(Z)}{P(Z, X)} + log \ P(X)\right] \\ &= \sum_{Z}Q(Z)[log Q(Z) - log P(Z,X)] + \sum_{Z}Q(Z)[log P(X)] \end{align*}$$

$P(X)$ is a constant w.r.t $Z$ and $\sum_{Z}Q(Z) = 1$ as $Q(Z)$ is a distribution. So:

$$ D_{KL}(Q || P) = \mathbb{E}_Q[log Q(Z) - log P(Z,X)] + log P(X) $$

And thus we have the marginal distribution of the evidence all by itself

$$ log P(X) = D_{KL}(Q || P) - \mathbb{E}_Q[log Q(Z) - log P(Z,X)] $$

Therefore, **to minimize $D_{KL}(Q || P)$, as $P(X)$ is fixed, you need to maximize $-\mathbb{E}_Q[log Q(Z) - log P(Z,X)]$**
(Notice the minus sign in front!)

An appropriate choice of $Q$ makes $-\mathbb{E}_Q[log Q(Z) - log P(Z,X)]$ tractable to compute, and to maximize.

This term is known as the Evidence Lower Bound. So we have $Q$ which is the approximation for the posterior, and a lower bound for $logP(X)$ Woop woop! 

$$ \begin{align*} \mathcal{L}(Q) &= -\mathbb{E}_Q[log Q(Z) - log P(Z,X)] \\ &= H(Q) - H(Q; P(X, Z))\\
&= - \sum_{Z}Q(Z)logQ(Z) + \sum_{Z}Q(Z)logP(Z,X)  \end{align*}$$
