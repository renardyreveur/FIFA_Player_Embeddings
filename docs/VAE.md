# Autoencoding Variational Bayes (VAE)

## Setting the Scene

Say some dataset $X = \{x^{(i)}\}_{i=1}^{N}$ consisting of $N$ i.i.d samples of some variable $x$ exists.

Assume that the ***data is generated*** through a ***Random Process*** involving an unobserved random variable *z*

(1) $z^{(i)}$  generated from some prior $p_{\theta}(z)$, this is something that we can assume and choose.

(2) $x^{(i)}$ generated from conditional $p_{\theta}(x | z)$ which is also something that we can assume and choose.

Let's assume the two distributions from (1) and (2) are from the same distribution family.

> Note: $\theta$ and $z$ are unknown to us

<br>

---

<br>

## What are we interested in?

(i) Maximum Likelihood or Max A Posteriori **estimate of $\theta$** (the parameter themselves) which are the generating parameters. We can imitate the random process the data is generated from and create more data on-demand.

(ii) A good *approximate* **posterior inference of latent variable $z$**, given an observed $x$ and a choice of $\theta$. This is the recognition model or the **encoder**: $q_\phi(z|x)$. This can be used for representation, dimensionality reduction, embeddings.

(iii) Efficient **approximate marginal inference of $x$**, $p_\theta(x)$. Inference tasks requiring prior over $x$ such as in computer vision (image denoising, inpainting super resolution, etc) are use cases.

$$ p_\theta(x) = \int_z p_\theta(x|z) p_\theta(z) dz$$

The integral above is usually intractable, so that presents the need for an *approximation* for $p_\theta(x)$. $p_\theta(x|z)$ here is the probabilistic decoder (given code $z$, produce distribution over possible $x$)

<br>

---

<br>

## How are we going to attain those?

The context in which we are trying to acquire those values we are interested in is through an *iterative learning algorithm*. Specifically, the usage of a **neural network** as the parameterized probabilisitc process is key in our investigation.

In order to use an *iterative learning algorithm* in reaching our goal, we have several considerations to make.

- The neural network's parameters are updated through calculating the gradient of the loss function with respect to each of it's parameters. The calculation of the gradients are done through an algorithm called 'back-propagation', and thus **the parameters in the path of 'back-propagation' have to be deterministic and differentiable**. 

- Integrals and other intractable calculations have to be numerically simulated through batches of samples. This can mean **an approximation of integrals with a Monte Carlo estimator can provide a good view of the problem.**

- We need to develop a 'loss function' for the neural network to be optimized. The choice of *loss function is related to 'how' we want our parameterized learning algorithm to shape up*. Thus, in our attempt to approximate certain properties of the randomly generated dataset such as the posteriors, and priors on the latent variable or samples, **utilizing the theory of variational bayes, and how it relates to the design of the neural network is crucial.**

With that in mind, let us remind ourselves with the concept of variatonal bayes in context of the setting of this problem. 

We have a dataset that is composed of observed samples that were generated from a random variable $x$. This $x$ is thought to have been derived from a random process which involved another random, unobserved variable $z$. Realizations of $z^{(i)}$ is thought to come into place from a parameterized probability distribution on $z$, and for each given $z$ another function, also parameterized by the same parameters that brought $z$ into existence, creates that sample $x^{(i)}$. (For a $x$ to have come into existence given a $z$, they must be of the same parametric family, or else those two are mutually exclusive variables, not affecting each other with their existence.)

So here, **what we are actually interested in is $z$**. $z$ is something conceptual, maybe a (probabilistic) fact about the state of some phenomenon or physical quantity, etc. The problem is that we can't directly observe $z$, but only a related variable that we think has been generated from it. **How does the observation change our conceputal understanding of $z$? In other words, what is the posterior inference of latent variable $z$?** That is *the* question. 

Bayes Theorem gives us a framework in calculating the answer to the above as:

$$P(Z | X) = \frac{P(X|Z)P(Z)}{P(X)}$$

Here $P(Z)$ represents our current understanding(prior) of $z$ (as a probability distribution), so it's something that we know. $P(X|Z)$ is basically the (likelihood distribution of) **data** that we can calculate from the observed data, so it's something that we know as well. The likelihood distribution presents a different view of $z$ based on data as opposed to the prior distribution which is our current belief. The likelihood distribution and the prior distribution, weighted by their uncertainty, balance (act as regularizers to each other) to give the posterior distribution.

$P(X)$ is pretty hard to calculate, its not a distribution but a number, and if you think about it we need to know the probability of that sample happening out of all the samples that can happen, and that can be infinite. Typically we can marginalize over $Z$ and integrate, but that integration is also frequently intractable. So we either approximate this or only infer on values that are not dependent on this.

Summarizing we get:

- $P(Z)$ is the prior distribution, our current belief on a concept, we know this or at least we can say what we think it is.

- $P(X|Z)$ is the likelihood distribution, the new view given by the data that challenges our current belief. We directly observe the data, and we can assume (we don't have to) the distribution family it came from to be the same as the one as our prior, and create a likelihood distribution.

- $P(X)$ is the probability of that sample happening out of all samples, and thus it is usually unknown to us.

- We are interestd in $P(Z|X)$, and when Bayes Theorem calculations are intractable, we approximate with a variational distribution $Q(Z|X)$ or $Q(Z)$. $Q$ is typically from a different distribution family to $P$ to make calculations easier.

Thus in variational bayes we are finding $Q(Z|X) \approx P(Z|X)$, and as we want our approximate to be as close as possible, this creates an optimization problem. **AH GREAT WE NEEDED A LOSS FUNCTION FOR ITERATIVE OPTIMIZATION ANYWAYS!**

Using the (reverse) Kullback-Leibler divergence as the measure of similarity betweeen the approximation and the true posterior, we arrive at 

$$ D_{KL}(q_\phi(z|x) \ || \ p_\theta(z|x)) = log(p_\theta(x)) + \sum_{z}q_\phi(z|x)\left[log(q_\phi(z|x)) - log(p_\theta(z,x))\right] $$

and as $p(x)$ is fixed, minimizing the Kullback-Leibler divergence is equivalent to **maximizing the Evidence Lower Bound** given as

$$ ELBO  = - \sum_{z}q_\phi(z|x)\left[log(q_\phi(z|x)) - log(p_\theta(z,x))\right] $$

The change from $p_\theta(z|x)$ to $p_\theta(z, x)$ comes from the definition of conditional probability $p_\theta(z|x) = \frac{p_\theta(z,x)}{p_\theta(x)}$

This is in the form $-E_{q_\phi (z)} [f(z)]$, so a we can come up with a Monte Carlo estimator of the ELBO as 

$$ \frac{1}{L} \sum_{l=1}^{L}f(z^{(l)}) $$

Apparently gradient of this Monte Carlo estimator w.r.t $\phi$ the parameters of $q$ is high, so it's not suitable to directly use the Monte Carlo estimator.

---

Instead of the Monte Carlo estimate we can estimate the lower bound and its derivative through a learning algorithm by considering the topics discussed above. 

First to handle the random variables inside our potential loss function(ELBO), we reparameterize the random variables with a deterministic one with a dummy random noise variable. This isn't always possible, but a lot of distributions can be transformed like this under mild conditions.

An example is the Gaussian: let $z \sim p(z|x) = N(\mu, \sigma^2)$, a valid reparameterization is $ z = \mu + \sigma\epsilon$ where $ \epsilon \sim N(0,1)$

Then we can keep the Monte Carlo estimator of the ELBO as the loss function, and get the gradients through back-propagation.

$$ \mathcal{\tilde{L}}^A(\theta, \phi;x^{(i)}) = \frac{1}{L}\sum_{l=1}^{L}[log \ p_\theta(x^{(i)}, z^{(i,l)}) - log \ q_\phi(z^{(i,l)} | x ^{(i)})]$$

where $ z^{(i,l)} = g_\phi (\epsilon^{(i,l)}, x^{(i)})$ and $\epsilon^{(l)} \sim p(\epsilon)$. 

$g_\phi$ is a differentiable transformation that reparameterizes like the above. 

There is another way to formulate this:
 Substituting $p_\theta(x, z) = p_\theta(x|z)p_\theta(z)$ to the ELBO function above we get 

 $$ \mathcal{L}(\theta, \phi;x^{(i)}) = \sum_{z}q_\phi(z|x)\left[log(p_\theta(z)) -log(q_\phi(z|x)) + log(p_\theta(x|z)) \right]$$

 This is the same as

  $$ \mathcal{L}(\theta, \phi;x^{(i)}) = -D_{KL}(q_\phi(z|x) \ || \ p_\theta(z)) + E_{q_\phi}\left[log \ p_\theta(x|z) \right]$$

  There the KL divergence part is often tractable to integrate analytically, and if so, the only estimate we need to make is on the second term of the RHS, and thus result in less variance than $\mathcal{\tilde{L}}^A$.

  $$\mathcal{\tilde{L}}^B(\theta, \phi;x^{(i)}) = -D_{KL}(q_\phi(z|x) \ || \ p_\theta(z)) + \sum_{l=1}^{L}log \ p_\theta(x^{(i)}| z^{(i,l)})$$

  where $ z^{(i,l)} = g_\phi (\epsilon^{(i,l)}, x^{(i)})$ and $\epsilon^{(l)} \sim p(\epsilon)$.

  When we assume $p_\theta(z)$ and $p_\theta(x|z)$ to be a Gaussian and also approximate the posterior with a Gaussian ($q_\phi(z|x)$), we can use the second approximation. This is the base configuration of a Variational Auto Encoder.

  Given multiple datapoints from a dataset $X$ with $N$ datapoints, the ELBO for the full dataset, based on minibatches is

  $$\mathcal{L}(\theta, \phi;X) \approx \mathcal{\tilde{L}}^M(\theta, \phi ; X^M ) = N \times \frac{1}{M} \sum_{i=1}^{M}\mathcal{\tilde{L}}(\theta, \phi;x^{(i)}) $$

  where $X^M = \{x^{(i)}\}_{i=1}^{M}$, a randomly drwan sample of $M$ datapoints from full dataset $X$ with $N$ datapoints.

  When $M$ large enough $L$ can be as low as 1! (according to the author's experiments)

  > So here we have it! A suitable loss function to express variational bayes approximation (with a touch of Monte Carlo) of a posterior distribution.

  and with terms like $q_\phi(z|x)$ and $p_\theta(x|z)$ a neural network design like an autoencoder is suitable for the training process!

  >The **encoder will then learn to be come the approximation of the posterior**, the value of the **loss function the lower bound for the evidence**, and the **decoder a representation of the likelihood distribution**. How cool is that? **All parts of Bayes theorem in a neural network!**

  The vanilla auto encoder with the priors, likelihood, and approximate posterior modelled with a Gaussian will look like the following:

  ![VAE](VAE.jpg)

  The ELBO loss function will need a minus in front to change it from maximizing to minimizing!

  Looking at the terms in the ELBO loss function, we see the Kullback-Leibler divergence of the approximate posterior from the prior. This acts as a regularizer. The second term is the probability density of datapoint $x$ under the generative model given $z$, and such is the reconstruction error. The divergence here regularizes the reconstruction error such that the data driven values of $z$ are balanced by the prior.