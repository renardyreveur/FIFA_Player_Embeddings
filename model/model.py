import torch
import math
import torch.nn as nn

from base import BaseModel


class AEVB(BaseModel):
    def __init__(self, data_dim: int, latent_dim: int):
        super().__init__()
        self.data_dim = data_dim
        self.latent_dim = latent_dim

        # Encoder (Phi) and Decoder (Theta)
        compression_factor = math.floor(math.sqrt(self.data_dim // self.latent_dim))
        assert compression_factor >= 1

        encoder_list = []
        decoder_list = []
        for cf in range(compression_factor):
            encoder_list.append(nn.Sequential(
                nn.Linear(data_dim // 2 ** cf, data_dim // 2 ** (cf + 1)),
                nn.BatchNorm1d(data_dim // 2 ** (cf + 1)),
                nn.Hardswish()
            ))

            dlist = [nn.Linear(data_dim // 2 ** (cf + 1), data_dim // 2 ** cf)]
            if cf != 0:
                dlist.append(nn.BatchNorm1d(data_dim // 2 ** cf))
                dlist.append(nn.Hardswish())
            decoder_list.append(nn.Sequential(*dlist))

        self.encoder = nn.Sequential(*encoder_list)
        self.mu = nn.Linear(data_dim // 2 ** compression_factor, latent_dim)
        self.sigma = nn.Linear(data_dim // 2 ** compression_factor, latent_dim)

        decoder_list.append(
            nn.Sequential(
                nn.Linear(latent_dim, data_dim // 2 ** compression_factor),
                nn.BatchNorm1d(data_dim // 2 ** compression_factor),
                nn.Hardswish()
            ))
        self.decoder = nn.Sequential(*reversed(decoder_list))

    def encode(self, x):
        encx = self.encoder(x)
        mu, sigma = self.mu(encx), torch.exp(0.5*self.sigma(encx))
        z = mu + sigma * torch.randn_like(sigma)  # Re-parameterization of the Gaussian random variable
        return z, mu, sigma

    def decode(self, x):
        y = self.decoder(x)
        return y

    def forward(self, x):
        z, mu, sigma = self.encode(x)
        y = self.decode(z)
        return x, y, mu, sigma
