import torch.nn as nn


class conv_bn_relu(nn.Module):
    def __init__(self, in_ch, out_ch, kernel, stride=1, groups=1):
        super().__init__()
        padding = 0 if kernel == 1 else 1
        self.conv = nn.Conv2d(in_ch, out_ch, kernel, stride=stride, padding=padding, groups=groups)
        self.bn = nn.BatchNorm2d(out_ch)
        self.relu = nn.ReLU6()

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)
        x = self.relu(x)
        return x
