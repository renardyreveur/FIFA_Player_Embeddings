import torch
import torch.nn.functional as F


def ELBOLoss(output):
    x, y, mu, sigma = output

    # KL Divergence of approximate posterior from prior (When both are modelled with Gaussian)
    kl_div = torch.mean(-0.5 * torch.sum(1 + sigma - mu**2 - sigma.exp(), dim=1), dim=0)

    # Reconstruction loss is the mean of log likelihood of samples
    # recon_loss = torch.mean(torch.log(y))

    recon_loss = F.mse_loss(y, x)
    # Reconstruction loss based on MSE and Cross Entropy (Knowing what our data looks like)
    # TODO: MSE + CE loss

    return kl_div + recon_loss
