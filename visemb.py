import torch
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pandas.plotting import parallel_coordinates
from model import AEVB



def get_class(pos):
    indices = [idx for idx, el in enumerate(pos) if el == "1"]
    pos = [0, 0, 0, 0]
    pos_str = ["FW", 'MF', 'DF', 'GK']
    for ind in indices:
        if ind == 2 or ind == 5 or ind == 10 or ind == 11:
            pos[0] += 1
        elif ind == 3 or ind == 6 or ind == 7 or ind == 8 or ind == 13:
            pos[1] += 1
        elif ind == 1 or ind == 4 or ind == 9 or ind == 12 or ind == 14:
            pos[2] += 1
        else:
            pos[3] += 1

    return pos_str[pos.index(max(pos))]


with open("pl_above_70.csv", 'r') as f:
    data = f.readlines()

model = AEVB(50, 5)
ckpt = torch.load("saved/models/FIFA_VAE/0109_023554/checkpoint-epoch50.pth")

model.load_state_dict(ckpt['state_dict'])
model.eval()

stat_data = [x.strip().split(",")[:50] for x in data]
stat_data = [[float(y) if y != "" else 0 for y in x] for x in stat_data]
player_class = [x.strip().split(",")[-15:] for x in data]
player_class = [get_class(x) for x in player_class]

embs = []
for sd in stat_data:
    player = torch.unsqueeze(torch.from_numpy(np.asarray(sd, dtype=np.float32)), 0)
    embs.append(model.encode(player)[0][0].detach().numpy())

emb_df = pd.DataFrame(np.asarray(embs))
emb_df['class'] = player_class
print(emb_df)

parallel_coordinates(emb_df, "class", colormap=plt.get_cmap("viridis"))
plt.show()
