player_positions
overall
potential
wage_eur
age
dob
height_cm
weight_kg
club_team_id
league_name
nationality_id
nation_team_id
preferred_foot
weak_foot
skill_moves
international_reputation
work_rate
body_type
player_tags
player_traits
pace
shooting
passing
dribbling
defending
physic
attacking_crossing
attacking_finishing
attacking_heading_accuracy
attacking_short_passing
attacking_volleys
skill_dribbling
skill_curve
skill_fk_accuracy
skill_long_passing
skill_ball_control
movement_acceleration
movement_sprint_speed
movement_agility
movement_reactions
movement_balance
power_shot_power
power_jumping
power_stamina
power_strength
power_long_shots
mentality_aggression
mentality_interceptions
mentality_positioning
mentality_vision
mentality_penalties
mentality_composure
defending_marking_awareness
defending_standing_tackle
defending_sliding_tackle
goalkeeping_diving
goalkeeping_handling
goalkeeping_kicking
goalkeeping_positioning
goalkeeping_reflexes
goalkeeping_speed







player_positions
{'LW', 'GK', 'CAM', 'LM', 'CB', 'LWB', 'RWB', 'RM', 'RB', 'CM', 'ST', 'CF', 'RW', 'LB', 'CDM'}

player_tags
{'#Tackling', '#Engine', '#Clinical Finisher', '#Dribbler', '#Speedster', '#Poacher', '#Complete Midfielder', '#Strength', '#Complete Forward', '#Playmaker', '#Complete Defender', '#Tactician', '#FK Specialist', '#Crosser', '#Distance Shooter', '#Aerial Threat', '#Acrobat'}

player_traits
{'Early Crosser', 'Long Passer (AI)', 'Dives Into Tackles (AI)', 'Power Header', 'Finesse Shot', 'Playmaker (AI)', 'Speed Dribbler (AI)', 'Giant Throw-in', 'Flair', 'Injury Prone', 'Comes For Crosses', 'GK Long Throw', 'Cautious With Crosses', 'One Club Player', 'Rushes Out Of Goal', 'Technical Dribbler (AI)', 'Leadership', 'Power Free-Kick', 'Outside Foot Shot', 'Long Throw-in', 'Saves with Feet', 'Solid Player', 'Team Player', 'Chip Shot (AI)', 'Long Shot Taker (AI)'}